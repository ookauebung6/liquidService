package de.hbrs.liquidservice.kafka.consumer.configuration;

import de.hbrs.liquidservice.LiquidServiceApplication;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
public class KafkaConfigurationConsumer {

    private LiquidServiceApplication liquidServiceApplication;

    public KafkaConfigurationConsumer (LiquidServiceApplication liquidServiceApplication) {
        this.liquidServiceApplication = liquidServiceApplication;
    }

    @KafkaListener(topics = "ooka_hauserweber_configuraton_liquidservice", groupId = "ookahauserweber.wirschiffendas.liquid_consumer")
    public void listenConfiguration (Configuration configuration) {
        System.out.println("Kafka: Message eingegangen");
        liquidServiceApplication.checkConfig(configuration);
    }
}
