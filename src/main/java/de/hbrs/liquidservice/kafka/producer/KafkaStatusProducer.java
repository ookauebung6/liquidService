package de.hbrs.liquidservice.kafka.producer;

import de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier;
import de.hbrs.wirschiffendas.data.entity.Status;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaStatusProducer {

    @Autowired
    private KafkaTemplate<String, StatusTransferItem> kafkaTemplate;

    public void sendStatus (Status newStatus) throws Exception {
        StatusTransferItem statusTransferItem = new StatusTransferItem(AlgorithmIdentifier.LIQUID, newStatus);
        new KafkaProducerConfig().kafkaTemplate().send("ooka_hauserweber_status", statusTransferItem);
    }

}
